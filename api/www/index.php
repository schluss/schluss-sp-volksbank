<?php

// SETTINGS -------------------------------

/* 	Private key file location
	This needs to be the private key part of a 2048bit GPG key pair
	DO NOT PUT THIS IN A PUBLIC FOLDER LIKE IN THIS DEMO
 */
 
	$PUBLICKEYPATH = dirname(__FILE__) . '/../keys/volksbank_schluss_pubkey_pgp.asc';
	$PRIVATEKEYPATH = dirname(__FILE__) . '/../keys/volksbank_schluss_privkey_pgp.asc';

/* 	File storage location
	The path where the flat files for this demo will be stored.
	Normally a decent backend system / database is used of course!
 */
 
	$STORAGEPATH = dirname(__FILE__) . '/../store/';

// Error reporting 
	error_reporting(E_ALL);


/// INIT -------------------------------

// load ext libraries
require_once('../vendor/autoload.php');

// create router instance
$router = new Router($prefix = '/api');


/// ROUTES -------------------------------

/**
 * @api {get} /token 
 * Get a new token (unique identifier for the whole session, all interactions with the Schluss user)
 *
 * @return {json} token
 */
$router->get('/token', function(){

	$token = uuidv4();
	
	Storage::set($token, 'state', 'new');

	http_response_code(200);
	return ['token' => $token];	
	
});

/**
 * @api {get} /publickey 
 * Get the data requesters' public key
 *
 * @return {text/plain} armored public key
 */
$router->get('/publickey', function(){
	
	global $PUBLICKEYPATH;
	
	http_response_code(200);
	
	header('Content-Type: text/plain');

	echo file_get_contents($PUBLICKEYPATH);
	
	exit();
	
});

/**
 * @api {get} /state 
 * Get the current 'connection' state
 *
 * @param {string} (Header) Auhthorization Must contain session token 
 *
 * @return {json} Result status message
 */
$router->get('/state', function(){
	
	// get the token
	$token = $_SERVER["HTTP_AUTHORIZATION"];
	
	// check if token is not empty
	if (empty($token)) {
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}
	
	// token existence check
	if (!Storage::exists($token, 'state')) {	
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token', 'token' => $token];
	}	
	
	http_response_code(200);
	
	$state = Storage::get($token, 'state');
	
	return [
		'message' => 'ok', 
		'token' => $token , 
		'state' => $state];
});


/**
 * @api {post} /state 
 * Update the current 'connection' state
 *
 * @param {string} (Header) Auhthorization Must contain session token 
 *
 * @return {json} Result status message
 */
$router->post('/state', function(){
	
	// get the token
	$token = $_SERVER["HTTP_AUTHORIZATION"];
	
	// check if token is not empty
	if (empty($token)) {
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}
	

	// token existence check, normally token is generated server side
		// this is a quick bypass to store the token state
	
	if (!Storage::exists($token, 'state')) {	
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}	
	
	// get the json body
	$jsonBody = getJSON();
			
	// store the new status
	Storage::set($token, 'state', $jsonBody['state']);
		
	http_response_code(200);
	
	return [
		'message' => 'ok', 
		'description' => 'State updated', 
		'token' => $token , 
		'state' => $jsonBody['state']];
});


/**
 * @api {get} /contract 
 * Get the stored contract. 
 * Note, due to decryption in this part of the process, this method can take some time
 *
 * @param {string} (Header) Auhthorization Must contain session token 
 *
 * @return {json} Decrypted contract data
 */
$router->get('/contract', function(){

	global $PRIVATEKEYPATH;
	
	// higher time-limit (5 mins), decrypting will take some time on some machines!
	set_time_limit(300);	

	// get the token
	$token = $_SERVER["HTTP_AUTHORIZATION"];

	// check if token is not empty
	if (empty($token)) {
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}
	
	// token existence check
	if (!Storage::exists($token, 'state')) {	
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}		
	
	// load private key:
	$privKeyData = file_get_contents($PRIVATEKEYPATH);
	$privKeyData = str_replace(chr(0), "", $privKeyData); // bug in file_get_contents does not process the armored ascii string correctly, see: https://www.php.net/manual/en/function.file-get-contents.php#76358

	$PrivKey = OpenPGP_Message::parse(OpenPGP::unarmor($privKeyData, 'PGP PRIVATE KEY BLOCK'));

	// get contract from storage
	$contract = Storage::get($token, 'contract');
	
	// remove stored data (to clean up after testing and te overcome data/security breaches)
	Storage::remove($token, 'state');
	Storage::remove($token, 'contract');	

	// read message
	$message = json_decode($contract);
	
	// decrypt contract from message
	$decryptor = new OpenPGP_Crypt_RSA($PrivKey);
	$decrypted = $decryptor->decrypt(OpenPGP::unarmor($message->contract,'PGP MESSAGE'));	
	
	// whole contract	
	$data = $decrypted->packets[1]->data;
	
	// decrypt individual attributes:
	$contractObj = json_decode($data);
	
	http_response_code(200);
	
	return [
		'message' => 'ok', 
		'token' => $token, 
		'contract' => $contractObj];
	
	
});

/**
 * @api {post} /contract 
 * Store the contract, recieved from the Schluss app
 *
 * @param {string} (Header) Auhthorization Must contain session token 
 *
 * @return {json} Result status message
 */
$router->post('/contract', function(){
	
	// get the token
	$token = $_SERVER["HTTP_AUTHORIZATION"];
	
	// check if token is not empty
	if (empty($token)) {
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}
	
	// token existence check
	if (!Storage::exists($token, 'state')) {	
		http_response_code(401);
		return ['message' => 'error', 'description' => 'Invalid token'];
	}
	
	// get the json body
	$jsonBody = getJSON();	
	
	// store the whole json body 'as contract' 
	Storage::set($token, 'contract', json_encode($jsonBody));
	
	// update the state to 'connected'
	Storage::set($token, 'state', 'connected');
	
	http_response_code(201);
	
	return [
		'message' => 'ok', 
		'description' => 'Created', 
		'token' => $token];		
});

/**
 * @api {get} /non-existing-api-url 
 * A non existing url is being requested
 *
 * @return {json} 404 error message
 */
$router->on('404', function(){
	http_response_code(404);
	return ['message' => 'error', 'description' => 'method not found'];
});


// run the router
$router->run();


/// -------------------------------

// Generate UUID
function uuidv4() {
    $data = PHP_MAJOR_VERSION < 7 ? openssl_random_pseudo_bytes(16) : random_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // Set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // Set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

// Get JSON data from POST request
function getJSON()
{
	return json_decode(file_get_contents('php://input'), true);
}

class Storage {

	static function exists($id, $type) {
		
		global $STORAGEPATH;
		
		$filename = $STORAGEPATH . $id . '_'.$type.'.txt';
		
		if (!file_exists($filename))
			return false;
		
		return true;
	}

	static function get($id, $type) {
		
		global $STORAGEPATH;
		
		$filename = $STORAGEPATH . $id . '_'.$type.'.txt';
		
		if (!self::exists($id, $type))
			return '';
		
		return file_get_contents($filename);
	}

	static function set($id, $type, $data) {
		
		global $STORAGEPATH;
		
		$filename = $STORAGEPATH . $id . '_'.$type.'.txt';
		$fp = fopen($filename, 'w+');
		fwrite($fp, $data);
		fclose($fp);
	}
	
	static function remove($id, $type){
		if (!self::exists($id, $type))
			return true;
		
		$filename = $STORAGEPATH . $id . '_'.$type.'.txt';
		
		return unlink($filename);
		
	}

}

class Router {
	
	private $routes = [];
	private $path;
	private $params;
	private $requestMethod;
	
	public function __construct($prefix = null) {
		
		$url = parse_url($_SERVER['REQUEST_URI']);

		$this->path = rtrim($url['path'], '/');

		if (!empty($prefix))
			$this->path = str_replace($prefix, '', $this->path);
		
		parse_str($url['query'], $this->params);	

		$this->requestMethod = $_SERVER['REQUEST_METHOD'];
	}
	
	public function add($method, $route, $fn) {
		$this->routes[$method][] = new Route($method, $route, $fn);
	}
	
	public function get($route, $fn) {
		$this->add('GET', $route, $fn);
	}	
	
	public function post($route, $fn) {
		$this->add('POST', $route, $fn);
	}


	public function on($event, $fn) {
		$this->add('on', '/404', $fn);
	}

	protected function find($method, $path) {
		foreach ($this->routes[$method] as $route) {
			if ($route->match($path))
				return $route;
		}
		
		return false;
	}
	
	public function run() {
		
		$return = '';
		
		try {
		
			$route = $this->find($this->requestMethod, $this->path);

			if ($route !== false) {
				$return = $route->run($this->params);
			}
			else {
				$on404 = $this->find('on', '/404');
				
				if ($on404 !== false)
					$return = $on404->run($this->params);				
			}
	
		}
		catch(Exception $e){
			
			$return = ['error' => 500, 'message' => $e->getMessage()];
		}
		
		if (!empty($return))
			$this->renderJSON($return);	 
	}
	
	public function renderJSON($json){
		
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/json');
		
		echo json_encode($json);
	}
}

class Route {

	private $method;
	private $route;
	private $fn;
	private $params;
	
	public function __construct($method, $route, $fn){
		$this->method = $method;
		$this->route = rtrim($route, '/');
		$this->fn = $fn;
	}
	
	public function match($path){
		
		$pattern = preg_replace('/\/{(.*?)}/', '/(.*?)', $this->route);
		
		
		if (preg_match_all('#^' . $pattern . '$#', $path, $matches, PREG_OFFSET_CAPTURE)) {
			// Rework matches to only contain the matches, not the orig string

			$matches = array_slice($matches, 1);
		
			// Extract the matched URL parameters (and only the parameters)
			$this->params = array_map(function ($match, $index) use ($matches) {
				// We have a following parameter: take the substring from the current param position until the next one's position (thank you PREG_OFFSET_CAPTURE)
				if (isset($matches[$index + 1]) && isset($matches[$index + 1][0]) && is_array($matches[$index + 1][0])) {
					return trim(substr($match[0][0], 0, $matches[$index + 1][0][1] - $match[0][1]), '/');
				} // We have no following parameters: return the whole lot
				return isset($match[0][0]) ? trim($match[0][0], '/') : null;
			}, $matches, array_keys($matches));
			
			return true;
		}

		return false;
	}
	
	public function run($params){

		if (is_callable($this->fn)){
			
			$p = array_merge($this->params, $params);
			
			return call_user_func_array($this->fn, $p);
		}
	}
}

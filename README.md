# Schluss service provider - Volksbank

This example demonstrates a basic implementation of a data requesting service provider with de Volksbank as subject. To be able to do data requests two components are needed: an api (for handling transactions between Schluss user and data requester) and a webbased front-end (to generate a QR code). How these two components work is explained below.

More information about how Service providers integrate within the Schluss ecosystem can be found here: [Schluss Serviceprovider](https://gitlab.com/schluss/schluss-serviceprovider/-/blob/master/README.md)

## Front-end
An example webpage that shows a QR code. The QR code contains all information to handle a data request from the data requesting party, collect the data via plugins inside the Schluss app Schluss and what api endpoint to use to share the resulting data to. The journey for Schluss users start when they scan the QR code.

Example code is located here:
[web/](./web/)

### Create a config.json

A config.json file defines a data request. Although this is not documented for now, some examples can be found here: [Schluss services directory](https://services.schluss.app/).

### Showing a QR generated code

To be able to show a QR code you must implement [schluss-connect](https://gitlab.com/schluss/schluss-connect).

An example implementation can be found here: [web/hypotheek.html](./web/hypotheek.html)

### Show status updates to the user

This example demonstrates a basic form of displaying progress messages to the Schluss user after scanning the QR code. This can be used to guide a user though the data request process.

An example implementation can be found here: [web/hypotheek.html](./web/hypotheek.html)


## API
An example API, which shows the needed endpoints to be able to retrieve the requested data via the users' Schluss app.

Example code is located here:
[api/](./api/)

### Methods

#### [GET] /token
Generate and return a new session token. Normally this is done internally by (another) backend system. This example is purely to demonstrate the usage of the token.

**Response**
Code 200:
`content type: aplication/json`
```json

{
	"token": "uuid4-formatted-token"
}
```
#### [GET] /publickey
Get the armored public key of the data requesting party

**Response**
Code 200:
`content type: text/plain`
```

-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFzG/iwBEACrEfIgZpBWRS2htejizPxh+FsqQGlGczmus9Pbqo33wCDJ0sEC
...
...
-----END PGP PUBLIC KEY BLOCK-----
```
#### [GET] /state
Get the current request state. States are sent from the Schluss App to the api to allow the web interface (containing the QR code) to update it's content according to the Schluss users' request progress. There are three possible states returned:
`new`: it's a newly generated / blank session.
`connecting`: The user has loaded the request and is collecting the requested data
`connected`: The user has finished and pushed the collected data to the api 

**Header:**
`Authorization: session_token`

**Response**
Code 200:
`content type: aplication/json`
```json

{
	"message":"ok",
	"token":"session_token",
	"state":"new|connecting|connected"
}
```
Code 401:
`content type: aplication/json`
```json
{
	"message":"error",
	"description":"Invalid token"
}
```

#### [POST] /state
Update the current request state. States are sent from the Schluss App to the api to allow the web interface (containing the QR code) to update it's content according to the Schluss users' request progress. There are two possible states that can be received from the Schluss app:
`connecting`: The user has loaded the request and is collecting the requested data
In future versions of the Schluss app more detailed state updates will be possible.

**Header:**
`Authorization: session_token`

**Response**
Code 200:
`content type: aplication/json`
```json
{
	"message":"ok",
	"description":"State updated",
	"token":"session_token",
	"state":"new|connecting|connected"
}
```
Code 401:
`content type: aplication/json`
```json
{
	"message":"error",
	"description":"Invalid token"
}
```

#### [GET] /contract
Get the stored contract. In a production environment this should not be public! This is pure for demonstration purposes.

**Header:**
`Authorization: session_token`

**Response**
Code 200:
`content type: aplication/json`
```json
{
	"message":"ok",
	"token":"session_token",
	"contract": {
		"requester": "[data requesting party name]",
		"provider": "[data providing party name]",
		"scope": "[scope of the data request]",
		"date": "2020-03-02T15:08:16.110Z",
		"shares": {
			"plugin-abc": [
				{
					"name": "attribute_x_name",
					"value": "attribute_x_value"
				},
				{
					"name": "attribute_y_name",
					"value": "attribute_y_value"
				}],
			"plugin-def": [
				{
					"name": "attribute_z_name",
					"value": "attribute_z_value"
				}],
		},
		"version": "0.0.1"
	}
}
```
Code 401:
`content type: aplication/json`
```json
{
	"message":"error",
	"description":"Invalid token"
}
```
#### [POST] /contract
Store the contract, received from the Schluss app. This method should (/could) also update the state to `connected`. 

**Header:**
`Authorization: session_token`

**Body**
`content type: aplication/json`
```json
{
	"contract": "[encrypted payload, decrypt with data requesting party private key]",
	"signingKey" : "[Schluss users' public key]"
}
```

**Response**
Code 200:
`content type: aplication/json`
```json
{
	"message":"ok",
	"description":"Created",
	"token":"session_token"
}
```
Code 401:
`content type: aplication/json`
```json
{
	"message":"error",
	"description":"Invalid token"
}
```